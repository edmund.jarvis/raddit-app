<?php

namespace Raddit\AppBundle\Utils;

use League\CommonMark\CommonMarkConverter;

/**
 * Utility class for formatting user-inputted Markdown.
 *
 * Unfortunately, the league/commonmark library is not safe for user input on
 * its own, even with the safety options. We could write custom parser and
 * renderer classes to try and make it safe, or we could just use HTMLPurifier
 * which, in addition to making input safe, gives us desired functionality like
 * autolinking and adding `target=_blank` to links.
 */
class MarkdownConverter {
    /**
     * @var CommonMarkConverter
     */
    private $converter;

    /**
     * @var \HTMLPurifier
     */
    private $purifier;

    /**
     * Creates an instance with the default configuration.
     *
     * @return self
     */
    public static function createInstance() {
        $commonMark = new CommonMarkConverter([
            'html_input' => 'escape',
        ]);

        $purifier = new \HTMLPurifier(\HTMLPurifier_Config::create([
            // Convert non-link URLs to links.
            'AutoFormat.Linkify' => true,
            // Disable cache
            'Cache.DefinitionImpl' => null,
            // Add rel="nofollow" to outgoing links.
            'HTML.Nofollow' => true,
            // Add target="_blank" to outgoing links.
            'HTML.TargetBlank' => true,
            // Disable embedding of external resources like images.
            'URI.DisableExternalResources' => true,
        ]));

        return new self($commonMark, $purifier);
    }

    /**
     * @param CommonMarkConverter $converter
     * @param \HTMLPurifier       $purifier
     */
    public function __construct(CommonMarkConverter $converter, \HTMLPurifier $purifier) {
        $this->converter = $converter;
        $this->purifier = $purifier;
    }

    /**
     * @param string $markdown
     *
     * @return string
     */
    public function convertToHtml($markdown) {
        $html = $this->converter->convertToHtml($markdown);

        $html = $this->purifier->purify($html);

        return $html;
    }
}
