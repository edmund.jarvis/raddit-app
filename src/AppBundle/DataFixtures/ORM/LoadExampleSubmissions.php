<?php

namespace Raddit\AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Raddit\AppBundle\Entity\Forum;
use Raddit\AppBundle\Entity\Submission;
use Raddit\AppBundle\Entity\User;

class LoadExampleSubmissions implements FixtureInterface, OrderedFixtureInterface {
    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager) {
        $forum = $manager->getRepository(Forum::class)->findOneBy(['name' => 'liberalwithdulledge']);
        $emma = $manager->getRepository(User::class)->findOneByUsername('emma');
        $zach = $manager->getRepository(User::class)->findOneByUsername('zach');

        $url = Submission::create($forum, $zach);
        $url->setTitle('This is a submitted URL');
        $url->setUrl('http://www.example.com');
        $manager->persist($url);

        $post = Submission::create($forum, $emma);
        $post->setTitle('This is a test submission');
        $post->setBody('Hi');
        $manager->persist($post);

        $combo = Submission::create($forum, $emma);
        $combo->setTitle('This is a submission with both a URL and a body');
        $combo->setUrl('http://www.example.org/some_thing_here');
        $combo->setBody('The quick brown fox jumped over the lazy dog.');
        $manager->persist($combo);

        $manager->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function getOrder() {
        return 2;
    }
}
